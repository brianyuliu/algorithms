from example_lists import *

# GOOD TO GO -> 1) selection  2) bubble  3) merge  4) insertion  5) quick
# TO DO -> 1)bucket  2) counting and radix

#-------------------------------------------------------------------------------------------------------

def swap(example_list, one, two):
    example_list[one], example_list[two] = example_list[two], example_list[one]
    return example_list


#-------------------------------------------------------------------------------------------------------

def selection_sort(example_list)-> list:
    for i in range(len(example_list)):
        minimum_index = i
        for c in range(i+1, len(example_list)):
            if example_list[c] < example_list[minimum_index]:
                minimum_index = c
        example_list = swap(example_list, i, minimum_index)
    return example_list

#-------------------------------------------------------------------------------------------------------

def bubble_sort(example_list)-> list:
    for i in range(0, len(example_list)):
        changed = 0
        for c in range(0, len(example_list)-1):
            if example_list[c] > example_list[c+1]:
                example_list = swap(example_list, c, c+1)
                changed = 1
        if changed == 0:
            break
    return example_list

#-------------------------------------------------------------------------------------------------------

def merge(list_one, list_two)-> list:
    merged = list()
    while list_one and list_two:
        if list_one[0] < list_two[0]:
            merged.append(list_one.pop(0))
        elif list_two[0] < list_one[0]:
            merged.append(list_two.pop(0))
        else:
            merged.append(list_one.pop(0))
            merged.append(list_two.pop(0))
    # at this point, only one list_half has elms left, so add all remaining elms to merged
    while list_one:
        merged.append(list_one.pop(0))
    while list_two:
        merged.append(list_two.pop(0))
    return merged

def merge_sort(example_list)-> list:
    if len(example_list) == 1:
        return example_list
    first_half = example_list[0 : len(example_list)//2]
    second_half = example_list[len(example_list)//2 : len(example_list)]
    return merge(merge_sort(first_half), merge_sort(second_half))

#-------------------------------------------------------------------------------------------------------

def insertion_sort(example_list)-> list:
    for i in range(1, len(example_list)):
        while(i-1 >= 0 and example_list[i] < example_list[i-1]):
            swap(example_list, i, i-1)
            i-=1
    return example_list
              
#-------------------------------------------------------------------------------------------------------

def bucket_sort(example_list)-> list:
    # make a 2d list that has a pretermined number of buckets
    buckets = list()
    number_of_buckets = 10
    pass    
              
#-------------------------------------------------------------------------------------------------------

def radix_sort(example_list)-> list:
    pass    
              
#-------------------------------------------------------------------------------------------------------
def partition(start, end, example_list)-> int:
    pivot_index = start # pivot index will be the first index of the input list
    pivot = example_list[pivot_index]

    while start < end:
        while start < len(example_list) and example_list[start] <= pivot: # increase start until hits first elm greater than pivot
            start +=1
        while example_list[end] > pivot: # decrease end until find (backwards) first elm less than pivot
            end-=1
        if start < end: # only swap if start and end have not crossed. if crossed, the while loop is done
            swap(example_list, start, end)
    # once the outer while loop is done, "end" will be right behind start(pointing at an elm < pivot), so swap "end" with pivot
    swap(example_list, pivot_index, end)
    # now, every elm before pivot is < pivot and every elm after pivot is > pivot, therefore pivot is in correct sorted place
    # return pivot index (which was swapped with end so return end) so that can recursively quicksort index 0 to pivot and quicksort pivot to end of list
    return end    


def quick_sort(start, end, example_list)-> list: # each time quick_sort is called or recursively called, one index (pivot) will be put in correct sorted place
    print("\n" + str(example_list))
    print(str(start) + "    " + str(end))
    if (start < end):
        pivot = partition(start, end, example_list) # after this is run, returned index p of partition() is in correct sorted place
        print("pivot index (after partition so in correct sorted place) -> " + str(pivot))
        quick_sort(start, pivot-1, example_list)
        quick_sort(pivot+1, end, example_list)
    return example_list

#-------------------------------------------------------------------------------------------------------

if __name__ == "__main__":
    print("\tWelcome to sort algorithms module!")
    print("\n\t-={ SORTED }=-")
    print("Initial List -> " + str(unsorted_list_one))
    print("Final List -> " + str(sorted(unsorted_list_one)))
    
#-------------------------------------------------

    print("\n\t-={ SELECTION SORTS O(n^2) aver, O(n^2) best, O(n^2) worst }=-")
    print("Initial List -> " + str(unsorted_list_one))
    print("Final List -> " + str(selection_sort(unsorted_list_one.copy())))

    print("\nInitial List -> " + str(unsorted_list_two))
    print("Final List -> " + str(selection_sort(unsorted_list_two.copy())))

    print("\nInitial List -> " + str(unsorted_list_three))
    print("Final List -> " + str(selection_sort(unsorted_list_three.copy())))
    
#-------------------------------------------------

    print("\n\t-={ BUBBLE SORTS O(n^2) aver, O(n) best, O(n^2) worst }=-")
    print("\tuse this when list is almost sorted bc would be fastest, but insertion sort still better")
    print("Initial List -> " + str(unsorted_list_one))
    print("Final List -> " + str(bubble_sort(unsorted_list_one.copy())))

    print("\nInitial List -> " + str(unsorted_list_two))
    print("Final List -> " + str(bubble_sort(unsorted_list_two.copy())))

    print("\nInitial List -> " + str(unsorted_list_three))
    print("Final List -> " + str(bubble_sort(unsorted_list_three.copy())))
    
#-------------------------------------------------
    
    print("\n\t-={ MERGE SORTS O(nlogn) aver, O(nlogn) best, O(nlogn) worst }=-")
    print("\tuse this when list is almost sorted bc would be fastest, but insertion sort still better")
    print("Initial List -> " + str(unsorted_list_one))
    print("Final List -> " + str(merge_sort(unsorted_list_one.copy())))

    print("\nInitial List -> " + str(unsorted_list_two))
    print("Final List -> " + str(merge_sort(unsorted_list_two.copy())))

    print("\nInitial List -> " + str(unsorted_list_three))
    print("Final List -> " + str(merge_sort(unsorted_list_three.copy())))

#-------------------------------------------------

    print("\n\t-={ INSERTION SORTS O(n^2) aver, O(n) best, O(n^2) worst }=-")
    print("Initial List -> " + str(unsorted_list_one))
    print("Final List -> " + str(insertion_sort(unsorted_list_one.copy())))

    print("\nInitial List -> " + str(unsorted_list_two))
    print("Final List -> " + str(insertion_sort(unsorted_list_two.copy())))

    print("\nInitial List -> " + str(unsorted_list_three))
    print("Final List -> " + str(insertion_sort(unsorted_list_three.copy())))

#-------------------------------------------------

    print("\n\t-={ BUCKET SORTS O(n+k) aver, O(n+k) best, O(n^2)worst }=-")
    print("Using insertion sort to sort the buckets")
    print("Initial List -> " + str(unsorted_list_one))
    print("Final List -> " + str(bucket_sort(unsorted_list_one.copy())))

    print("\nInitial List -> " + str(unsorted_list_two))
    print("Final List -> " + str(bucket_sort(unsorted_list_two.copy())))

    print("\nInitial List -> " + str(unsorted_list_three))
    print("Final List -> " + str(bucket_sort(unsorted_list_three.copy())))

#-------------------------------------------------

    print("\n\t-={ RADIX SORTS O(nk) aver, O(nk) best, O(nk) worst }=-")
    print("Initial List -> " + str(unsorted_list_one))
    print("Final List -> " + str(radix_sort(unsorted_list_one.copy())))

    print("\nInitial List -> " + str(unsorted_list_two))
    print("Final List -> " + str(radix_sort(unsorted_list_two.copy())))

    print("\nInitial List -> " + str(unsorted_list_three))
    print("Final List -> " + str(radix_sort(unsorted_list_three.copy())))

#-------------------------------------------------

    print("\n\t-={ QUICK SORTS O(nlog(n)) aver, O(nlog(n)) best, O(n^2) worst }=-")
    print("Initial List -> " + str(unsorted_list_one))
    print("------={ Final List -> " + str(quick_sort(0, len(unsorted_list_one)-1 , unsorted_list_one.copy())) + " }=------")

    print("\nInitial List -> " + str(unsorted_list_two))
    print("------={ Final List -> " + str(quick_sort(0, len(unsorted_list_two)-1, unsorted_list_two.copy())) + " }=------")

    print("\nInitial List -> " + str(unsorted_list_three))
    print("------={ Final List -> " + str(quick_sort(0, len(unsorted_list_three)-1, unsorted_list_three.copy())) + " }=------")
    
