from example_lists import *
from math import sqrt

#-------------------------------------------------------------------------------------------------------

''' global dictionary that maps the types of search algorithms we will be using to integer keys'''
algorithms_dict = {0: "Linear Search", 1: "Binary Search", 2: "Jump Search", 3:"Interpolation Search"}

'''
used for printing information that describe what time of search algorithm is used, input list, and what we're looking for
'''
def log(algorithm: int, example_list: list, find: int)-> None:
    print(algorithms_dict[algorithm] + " - Looking for " + str(find) + " - List - " + str(example_list))

#-------------------------------------------------------------------------------------------------------

'''
  used for unsorted data structures, because order is random. LEast info provided therefore slowest search algorithm
'''
def linear_search(example_list: list, find: int)-> str:
    for i in range(len(example_list)):
        if example_list[i] == find:
            return "Index of " + str(find) + " - " + str(i)
    return str(find) + " was not found in the example_list " + str(example_list)

#-------------------------------------------------------------------------------------------------------

'''
  used for sorted data structures for x by repeatedly dividing the search interval in half
  takes advantage of the given info that the alg is sorted, so will be quicker than linear search
'''
def binary_search(example_list: list, low: int, high: int, find: int)-> str:
    if high >= low:
        mid = int((high+low)/2)
        if example_list[mid] == find:
            return "Index of " + str(find) + " - " + str(mid)
        elif example_list[mid] > find:
            return binary_search(example_list, low, mid-1, find)
        return binary_search(example_list, mid + 1, high, find)
    else: # algorithm is over when low > high, one iteration after mid is the only elm left to check
        return str(find) + " was not found in the example_list " + str(example_list)
    
#-------------------------------------------------------------------------------------------------------

'''
  Also used for sorted data structures, so also quicker than linear search
  Idea is to check fewer elms than every elm in the structure (which would be linear search)
  jumps ahead by fixed steps instead of searching all elms
  Optimal fixed jump size is m=math.sqrt(n)
'''
def jump_search(example_list: list, find: int, jump: int)-> str:
    current_index = 0 
# if you are in the last possible block of list, next jump would throw error so break so linear search last block. else while find is bigger than biggest of current block, jump
    while current_index + jump < len(example_list) and find >= example_list[current_index + jump]: 
        current_index += jump
    for i in range(current_index, current_index + jump):
        if example_list[i] == find:
            return "Index of " + str(find) + " - " + str(i)
    return str(find) + " was not found in the example_list " + str(example_list)
    

#-------------------------------------------------------------------------------------------------------

'''
 An improvement over binary search for instances where the values in a sorted structure are uniformly
 distributed (LINEAR distribution)
 Binary search always goes to the middle elm to check but interpolation search may go to different locations
 according to the value of the searched key
 Interpolation search algorithm, if the prerequisites are met (sorted and linear), will be the quickest search algorithm
 because you have the most information to work with therefore the lowest time complexity
'''
def interpolation_search(example_list: list, low: int, high: int, find: int)-> str:
    if high >= low:
        mid = int( low + ( (high - low) // (example_list[high] - example_list[low]) * (find - example_list[low]) ) )
        print(mid)
        if example_list[mid] == find:
            return "Index of " + str(find) + " - " + str(mid)
        elif example_list[mid] < find:
            return interpolation_search(example_list, mid + 1, high, find)
        return interpolation_search(example_list, low, mid - 1, find)

    else: # because low < high, there are no more elms to possibly check therefore "find" is not in list
        return str(find) + " was not found in the example_list " + str(example_list)

#-------------------------------------------------------------------------------------------------------

if __name__ == "__main__":
    print("\tWelcome to search algorithms module!")
    
    print("\n\t-={ LINEAR SEARCHES O(n) }=-")
    log(0, unsorted_random_list, 73) 
    print(linear_search(unsorted_random_list, 73) + "\n")
    
    log(0, unsorted_random_list, 99) 
    print(linear_search(unsorted_random_list, 99)+ "\n")
    
    log(0, unsorted_random_list, 9) 
    print(linear_search(unsorted_random_list, 9) + "\n")
    
    log(0, unsorted_random_list, 6) 
    print(linear_search(unsorted_random_list, 6) + "\n")

    print("\t-={ BINARY SEARCHES O(log(n)) }=-")
    log(1, sorted_list, 13)
    print(binary_search(sorted_list, 0, len(sorted_list) - 1, 13) + "\n")

    log(1, sorted_list, 46)
    print(binary_search(sorted_list, 0, len(sorted_list) - 1, 46) + "\n")

    log(1, sorted_list, 1)
    print(binary_search(sorted_list, 0, len(sorted_list) - 1, 1) + "\n")

    log(1, sorted_list, 17)
    print(binary_search(sorted_list, 0, len(sorted_list) - 1, 17) + "\n")
    

    print("\n\t-={ JUMP SEARCHES O(sqrt(n)) }=-")
    print("\tOptimal fixed jump size is m=math.sqrt(n)")
    log(2, sorted_list, 13)
    print(jump_search(sorted_list, 13, int(sqrt(len(sorted_list)))) + "\n")
    
    log(2, sorted_list, 25)
    print(jump_search(sorted_list, 25, int(sqrt(len(sorted_list)))) + "\n")

    log(2, sorted_list, 1)
    print(jump_search(sorted_list, 1, int(sqrt(len(sorted_list)))) + "\n")

    log(2, sorted_list, 3)
    print(jump_search(sorted_list, 3, int(sqrt(len(sorted_list)))) + "\n")

    log(2, sorted_list, 46)
    print(jump_search(sorted_list, 46, int(sqrt(len(sorted_list)))) + "\n")

    log(2, sorted_list, 17)
    print(jump_search(sorted_list, 17, int(sqrt(len(sorted_list)))) + "\n")

    log(2, [1, 2, 3, 5, 7, 9, 13, 15, 18, 25], 25)
    print(jump_search([1, 2, 3, 5, 7, 9, 13, 15, 18,25], 25, 5) + "\n")
    
    

    print("\n\t-={ INTERPOLATION SEARCHES O(log(log(n))) }=-")
    log(3, linear_sorted_list, 5)
    print(interpolation_search(linear_sorted_list, 0, len(linear_sorted_list) - 1 , 5) + "\n")
    
    log(3, linear_sorted_list, 11)
    print(interpolation_search(linear_sorted_list, 0, len(linear_sorted_list) - 1, 11) + "\n")

    log(3, linear_sorted_list, 1)
    print(interpolation_search(linear_sorted_list, 0, len(linear_sorted_list) - 1, 1) + "\n")

    log(3, linear_sorted_list, 21)
    print(interpolation_search(linear_sorted_list, 0, len(linear_sorted_list) - 1, 21) + "\n")

    log(3, linear_sorted_list, 83)
    print(interpolation_search(linear_sorted_list, 0, len(linear_sorted_list) - 1, 83) + "\n")

    
